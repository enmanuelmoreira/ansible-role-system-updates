Ansible Role: System Updates
=========

**Perform upgrade of all software packages on Linux host.**

This Ansible role will perform upgrade of all software packages on Linux hosts.

* Update package cache
* Check if there are any available updates
* Perform upgrade of all packages to the latest version (dist)
* TODO: Check if a reboot is required, if it is reboot the host/server
* TODO: Wait for server to come back after reboot, and report once it's back-up and running.
  
Distros Supported:

* Debian
* Ubuntu  
* RHEL 7/8 - CentOS - AlmaLinux 8 - Rocky Linux 8
* Alpine
* Arch Linux - Manjaro
* SUSE >= 15 - Tumbleweed
* FreeBSD >= 12.0
* MacOSX (HomeBrew)
  
Requirements
------------

None.

Role Variables
--------------

None.

Dependencies
------------

None.

Example Playbook
----------------

```yml
- hosts: servers
  remote_user: ubuntu # optional
  gather_facts: yes
  become: yes

  roles:
    - { role: EnmanuelMoreira.system-updates }
```

License
-------

GPLv3
